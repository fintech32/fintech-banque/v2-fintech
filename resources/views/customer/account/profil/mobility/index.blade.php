@extends("customer.layouts.app")

@section("css")

@endsection

@section('toolbar')
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <!--begin::Title-->
        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Service Transbank</h1>
        <!--end::Title-->
    </div>
@endsection

@section("content")
    <div id="app" class="rounded container">
        <p class="fw-bolder fs-2">Transférez les virements et prélévements récurrents de votre ancien compte sur votre compte Société Générale</p>
        <div class="border border-2 rounded p-5 d-flex flex-column align-items-center mb-5">
            <ul class="list-unstyled">
                <li><strong>Une démarche simple:</strong> ous nous fournissez le RIB de votre ancien compte, formulez vos instructions en remplissant le formulaire ci-après et nous nous chargeons des formalités en votre nom.</li>
                <li><strong>Un suivi complet:</strong> à tout moment depuis votre Espace Clients, vous suivez la progression de votre mobilité bancaire</li>
            </ul>
        </div>
        <div class="d-flex flex-end mb-5">
            <button class="btn btn-circle btn-lg btn-outline btn-outline-dark" data-bs-toggle="modal" data-bs-target="#Subscribe">Souscrire un nouveau mandat</button>
        </div>
        <table class="table border table-row-bordered border-2 border-gray-300 table-striped gx-5 gy-5" id="tableMobilities">
            <thead>
                <tr>
                    <th>Désignation</th>
                    <th>Type de Transfer</th>
                    <th>Compte à compte</th>
                    <th>Etape</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($customer->mobilities as $mobility)
                    <tr>
                        <td>
                            <strong>{{ $mobility->name_mandate }}</strong><br>
                            <i>{{ $mobility->ref_mandate }}</i>
                        </td>
                        <td>{{ $mobility->type->name }}</td>
                        <td>
                            <div class="d-flex flex-row justify-content-center align-items-center">
                                <div class="d-flex flex-column">
                                    <strong>{{ $mobility->name_account }}</strong>
                                    <div class=""><strong>IBAN:</strong> {{ $mobility->iban }}</div>
                                    <div class=""><strong>BIC:</strong> {{ $mobility->bic }}</div>
                                </div>
                            </div>
                        </td>
                        <td>{!! $mobility->status_label !!}</td>
                        <td>
                            @if($mobility->status == 'select_mvm_bank' || $mobility->status == 'select_mvm_creditor')
                                <a href="{{ route('customer.account.profil.mobility.show', $mobility->ref_mandate) }}" class="btn btn-xs btn-icon btn-primary"><i class="fa-solid fa-eye text-white"></i> </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="modal fade" tabindex="-1" id="Subscribe">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-bank">
                    <h3 class="modal-title text-white">Création du mandat</h3>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa-solid fa-xmark text-white fs-1"></i>
                    </div>
                    <!--end::Close-->
                </div>

                <form id="formSubscribe" action="/api/user/{{ $customer->user->id }}/mobility" method="post">
                    @csrf
                    @method('POST')
                    <div class="modal-body">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <p>La mobilité bancaire concerne uniquement les comptes de dépôts. Les comptes d’épargne ainsi que les comptes titres, PEA sont exclus. Il n’est pas possible de faire une mobilité bancaire d’un compte joint (2 titulaires) vers un compte individuel (1 seul titulaire).</p>
                        <div class="d-flex flex-row justify-content-around">
                            @foreach(\App\Models\Core\MobilityType::all() as $type)
                                <x-form.radio-select
                                    name="mobility_type_id"
                                    label="{{ $type->name }}"
                                    label-content="{{ $type->description }}"
                                    value="{{ $type->id }}"
                                    icon="{{ \App\Helper\IconHelpers::getIcons()->random() }}"/>
                            @endforeach
                        </div>
                        <x-base.underline
                            title="Information sur l'ancien compte"
                            size-text="fs-1"
                            size="2"
                            class="w-auto my-5" />

                        <x-form.input
                            name="name_mandate"
                            label="Nom du mandat"
                            required="true" />

                        <x-form.input
                            name="name_account"
                            label="Nom du compte"
                            required="true" />

                        <x-form.input
                            name="iban"
                            label="IBAN"
                            required="true" />

                        <x-form.input
                            name="bic"
                            label="BIC"
                            required="true" />

                        <x-form.input
                            name="address"
                            label="Adresse Postal"
                            required="true" />

                        <x-form.input
                            name="addressbis"
                            label="Complément Adresse Postal" />

                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <x-form.input
                                    name="postal"
                                    label="Code Postal"
                                    required="true" />
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <x-form.input
                                    name="ville"
                                    label="Ville"
                                    required="true" />
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="mb-10">
                                    <label for="country" class="form-label required">Pays</label>
                                    <select name="country" id="country" class="form-control form-control-solid selectpicker">
                                        <option value=""></option>
                                        @foreach(\App\Helper\GeoHelper::getAllCountries() as $country)
                                            <option value="{{ $country->iso2 }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <x-form.select
                            name="customer_wallet_id"
                            label="Compte de reception"
                            required="true"
                            :datas="\App\Models\Customer\CustomerWallet::toSelect($customer->wallets()->where('status', 'active')->where('type', 'compte')->get())" />

                        <x-base.underline
                            title="Virements permanents"
                            size-text="fs-1"
                            size="2"
                            class="w-auto my-5" />
                        <p>La date que vous choisirez pourra être modifiée par {{ config('app.name') }} si la date de réception du mandat que vous avez signé ne permet pas de traiter vos demandes à la date mentionnée dans le mandat.</p>

                        <x-form.input-date
                            name="date_transfer"
                            label="Choisissez la date à laquelle vous souhaitez que vos virements permanents soient transférés sur votre nouveau compte :"
                            required="true" />

                        <div id="cloture_old_account">
                            <x-base.underline
                                title="Clôture du compte et transfert de solde"
                                size-text="fs-1"
                                size="2"
                                class="w-auto my-5" />
                            <p class="fs-9">La banque d'origine vous informera, pendant un délai de 13 mois à compter de la clôture du compte, de la présentation d'un virement, prélèvement ou paiement d'un chèque.
                                Le cas échéant, la banque d'origine vous informera des obligations en suspens ou de toute autre circonstance de nature à empècher le transfert de solde et la clôture du compte d'origine.
                                Une demande de clôture de votre compte au-delà de 6 mois à compter de la validation du mandat ne permet pas de bénéficier des  informations relatives aux opérations se présentant sur votre compte clos.
                                Si vous avez fait le choix de ne pas clôturer votre compte d'origine, nous attirons votre attention sur la nécessité d'approvisionner  suffisamment ce compte pour éviter tout incident de paiement.</p>

                            <x-form.checkbox
                                name="cloture"
                                label="Je souhaite clôturer mon compte d'origine et transférer le solde créditeur éventuel?"
                                value="1" />
                        </div>
                    </div>
                    <div class="modal-footer text-end">
                        <x-form.button />
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    @include("customer.scripts.account.profil.mobility.index")
@endsection
