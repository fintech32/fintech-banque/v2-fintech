<?php

namespace App\Http\Controllers\Admin\Configuration;

use App\Helper\LogHelper;
use App\Http\Controllers\Controller;
use App\Models\Core\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function index()
    {
        return view('admin.config.package.index', [
            'packages' => Package::all()
        ]);
    }

    public function store(Request $request)
    {
        dd($request->all());
        try {
            $package = Package::create($request->except('_token'));
            $package->update([
                'visa_classic' => $request->has('visa_classic'),
                'check_deposit' => $request->has('check_deposit'),
                'payment_withdraw' => $request->has('payment_withdraw'),
                'overdraft' => $request->has('overdraft'),
                'cash_deposit' => $request->has('cash_deposit'),
                'withdraw_international' => $request->has('withdraw_international'),
                'payment_international' => $request->has('payment_international'),
                'payment_insurance' => $request->has('payment_insurance'),
                'check' => $request->has('check'),
                'type_cpt' => $request->has('type_cpt'),
                'type_prlv' => $request->has('type_prlv'),
                'icon' => $request->has('icon'),
                'color' => $request->has('color'),
            ]);
        }catch (\Exception $exception) {
            LogHelper::notify('critical', $exception->getMessage(), $exception);
            return response()->json($exception, 500);
        }

        return response()->json($package);
    }
}
