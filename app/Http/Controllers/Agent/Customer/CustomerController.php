<?php

namespace App\Http\Controllers\Agent\Customer;

use App\Helper\CustomerHelper;
use App\Helper\CustomerLoanHelper;
use App\Helper\CustomerSepaHelper;
use App\Helper\CustomerWalletHelper;
use App\Helper\CustomerWalletTrait;
use App\Helper\DocumentFile;
use App\Helper\LogHelper;
use App\Helper\RequestHelper;
use App\Http\Controllers\Controller;
use App\Models\Core\Package;
use App\Models\Customer\Customer;
use App\Models\Customer\CustomerInsurance;
use App\Models\Customer\CustomerPret;
use App\Models\Insurance\InsurancePackageForm;
use App\Notifications\Customer\LogNotification;
use App\Notifications\Customer\NewContractInsuranceNotification;
use App\Notifications\Customer\NewPretNotification;
use App\Notifications\Customer\UpdateStatusAccountNotification;
use App\Notifications\Customer\UpdateStatusAccountNotificationP;
use App\Scope\CalcLoanInsuranceTrait;
use App\Scope\CalcLoanTrait;
use App\Services\Stripe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        return view('agent.customer.index', [
            "customers" => Customer::all()
        ]);
    }

    public function start()
    {
        return view('agent.customer.create.start');
    }

    public function finish(Request $request)
    {
        $session = (object) session()->all();
        $help = new CustomerHelper();

        if($request->has('refresh')) {
            $customer = Customer::find($request->get('customer_id'));
        } else {
            $create = $help->createCustomer($session);
            $customer = $create->customers;
            session()->forget('perso');
            session()->forget('rent');
            session()->forget('package');
            session()->forget('card');
            session()->forget('subscribe');
        }

        return view('agent.customer.create.finish', compact('customer'));
    }

    public function show($id)
    {
        $customer = Customer::find($id);

        return view('agent.customer.show', [
            'customer' => $customer
        ]);
    }

    public function update(Request $request, $customer_id)
    {
        $customer = Customer::find($customer_id);

        try {
            switch ($request->get('control')) {
                case 'address':
                    $customer->info->update([
                        'address' => $request->get('address') ? $request->get('address') : $customer->info->address,
                        'addressbis' => $request->get('addressbis') ? $request->get('addressbis') : $customer->info->addressbis,
                        'postal' => $request->get('postal') ? $request->get('postal') : $customer->info->postal,
                        'city' => $request->get('city') != null ? $request->get('city') : $customer->info->city,
                        'country' => $request->get('country') != null ? $request->get('country') : $customer->info->country,
                    ]);
                    break;

                case 'coordonnee':
                    $customer->user->update([
                        'email' => $request->get('email') && $customer->user->email !== $request->get('email') ? $request->get('email') : $customer->user->email,
                    ]);

                    $customer->info->update([
                        'phone' => $request->has('phone') && $customer->info->phone !== $request->get('phone') ? $request->get('phone') : $customer->info->phone,
                        'mobile' => $request->has('mobile') && $customer->info->mobile !== $request->get('mobile') ? $request->get('mobile') : $customer->info->mobile,
                    ]);
                    break;

                case 'situation':
                    $customer->situation->update([
                        'legal_capacity' => $request->has('legal_capacity') && $customer->situation->legal_capacity !== $request->get('legal_capacity') ? $request->get('legal_capacity') : $customer->situation->legal_capacity,
                        'family_situation' => $request->has('family_situation') && $customer->situation->family_situation !== $request->get('family_situation') ? $request->get('family_situation') : $customer->situation->family_situation,
                        'logement' => $request->has('logement') && $customer->situation->logement !== $request->get('logement') ? $request->get('logement') : $customer->situation->logement,
                        'logement_at' => $request->has('logement_at') && $customer->situation->logement_at !== $request->get('logement_at') ? $request->get('logement_at') : $customer->situation->logement_at,
                        'child' => $request->has('child') && $customer->situation->child !== $request->get('child') ? $request->get('child') : $customer->situation->child,
                        'person_charged' => $request->has('person_charged') && $customer->situation->person_charged !== $request->get('person_charged') ? $request->get('person_charged') : $customer->situation->person_charged,
                        'pro_category' => $request->has('pro_category') && $customer->situation->pro_category !== $request->get('pro_category') ? $request->get('pro_category') : $customer->situation->pro_category,
                        'pro_profession' => $request->has('pro_profession') && $customer->situation->pro_profession !== $request->get('pro_profession') ? $request->get('pro_profession') : $customer->situation->pro_profession,
                    ]);

                    $customer->income->update([
                        'pro_incoming' => $request->has('pro_incoming') && $customer->income->pro_incoming !== $request->get('pro_incoming') ? $request->get('pro_incoming') : $customer->income->pro_incoming,
                        'patrimoine' => $request->has('patrimoine') && $customer->income->patrimoine !== $request->get('patrimoine') ? $request->get('patrimoine') : $customer->income->patrimoine,
                    ]);

                    $customer->charge->update([
                        'rent' => $request->has('rent') && $customer->charge->rent !== $request->get('rent') ? $request->get('rent') : $customer->charge->rent,
                        'divers' => $request->has('divers') && $customer->charge->divers !== $request->get('divers') ? $request->get('divers') : $customer->charge->divers,
                        'nb_credit' => $request->has('nb_credit') && $customer->charge->nb_credit !== $request->get('nb_credit') ? $request->get('nb_credit') : $customer->charge->nb_credit,
                        'credit' => $request->has('credit') && $customer->charge->credit !== $request->get('credit') ? $request->get('credit') : $customer->charge->credit,
                    ]);
                    break;

                case 'communication':
                    $customer->setting->update([
                        'notif_sms' => $request->has('notif_sms') ? 1 : 0,
                        'notif_app' => $request->has('notif_app') ? 1 : 0,
                        'notif_mail' => $request->has('notif_mail') ? 1 : 0,
                    ]);
                    break;

                case 'status':
                    return $this->updateStatus($customer, $request);
                case 'type':
                    return $this->updateType($customer, $request);

            }
        }catch (\Exception $exception) {
            LogHelper::notify('critical', "Erreur Système", $exception->getMessage());
            return redirect()->back()->with('error', "Erreur lors de l'exécution de l'appel.<br>Contacter un administrateur.");
        }

        LogHelper::notify('notice', 'Mise à jours des informations du client: '.$customer->user->name);
        $customer->user->notify(new LogNotification('info', 'Vos informations personnel ont été mise à jours', null));
        return redirect()->back()->with('success', 'Les informations du client ont été mise à jours.');

    }

    public function createPret($customer_id)
    {
        $customer = Customer::find($customer_id);

        return view('agent.customer.wallet.pret.create', compact('customer'));
    }

    public function storePret($customer_id, Request $request)
    {
        $customer = Customer::find($customer_id);

        $wallet = CustomerWalletHelper::createWallet(
            $customer,
            'pret'
        );

        $wallet_payment = $customer->wallets()->find($request->get('wallet_payment_id'));

        // Création du Crédit
        $credit = $customer->prets()->create([
            'uuid' => Str::uuid(),
            'reference' => generateReference(10),
            'amount_loan' => $request->get('amount_loan'),
            'amount_du' => 0,
            'amount_interest' => 0,
            'mensuality' => 0,
            'prlv_day' => $request->get('prlv_day') ?? 20,
            'duration' => $request->get('duration') * 12,
            'assurance_type' => $request->get('assurance_type'),
            'customer_wallet_id' => $wallet->id,
            'wallet_payment_id' => $request->get('wallet_payment_id'),
            'first_payment_at' => null,
            'required_caution' => $request->has('required_caution'),
            'required_insurance' => $request->has('required_insurance'),
            'loan_plan_id' => $request->get('loan_plan_id'),
            'customer_id' => $customer_id
        ]);

        $amount_interest =  CalcLoanTrait::getLoanInterest($credit->amount_loan, $credit->plan->tarif->type_taux == 'fixe' ? $credit->plan->tarif->interest : CalcLoanTrait::calcLoanIntestVariableTaxe($credit));
        $amount_du = $request->get('amount_loan') + $amount_interest;
        $mensuality = $amount_du / ($request->get('duration') * 12);

        $credit->update([
            'amount_du' => $amount_du,
            'amount_interest' => $amount_interest,
            'mensuality' => $mensuality
        ]);


        for ($i=1; $i <= $credit->duration; $i++) {
            $amort = $credit->amortissements()->create([
                'customer_pret_id' => $credit->id,
                'date_prlv' => Carbon::create(now()->year, now()->month, $credit->prlv_day)->addMonths($i)->startOfDay(),
                'amount' => $credit->mensuality,
                'capital_du' => ($credit->amount_du-$credit->mensuality) / $i,
            ]);

            $sepa = CustomerSepaHelper::createPrlv(
                $credit->mensuality,
                $credit->payment,
                $credit->wallet->name_account_generic. " - Echéance {$amort->date_prlv->locale('fr')->monthName}",
                $amort->date_prlv,
            );

            $amort->update([
                'customer_sepa_id' => $sepa->id
            ]);
        }

        $contrat = DocumentFile::createDoc(
            $customer,
            'loan.contrat_de_credit_personnel',
            'Contrat de Credit Personnel',
            3,
            $credit->reference,
            true,
            true,
            false,
            true,
            ["pret" => $credit]
        );

        RequestHelper::create(
            $customer,
            "Signature d'un document",
            "Veuillez signer le document intitulé: {$contrat->name}",
            CustomerPret::class,
            $credit->id,
        );

        $mandat = DocumentFile::createDoc(
            $customer,
            'general.mandat_prelevement_sepa',
            'Mandat de prélèvement SEPA',
            3,
            $credit->reference,
            true,
            true,
            false,
            true,
            ['wallet' => $wallet_payment]
        );

        RequestHelper::create(
            $customer,
            "Signature d'un document",
            "Veuillez signer le document intitulé: {$mandat->name}",
            CustomerPret::class,
            $credit->id,
        );

        DocumentFile::createDoc(
            $customer,
            'loan.plan_damortissement',
            "Plan d'amortissement",
            3,
            $credit->reference,
            false,
            false,
            false,
            true,
            ["credit" => $credit]
        );

        if($credit->required_insurance){
            $insurance = $this->subscribeInsurance($customer, $credit, $request->get('assurance_type'));
            $credit->update([
                'customer_insurance_id' => $insurance->id
            ]);
        }

        $docs = [];
        foreach ($customer->documents()->where('reference', $credit->reference)->get() as $doc) {
            $docs[] = [
                'url' => public_path($doc->url_folder)
            ];
        }

        $customer->info->notify(new NewPretNotification($customer, $credit, $docs, "Prêt"));

        return redirect()->route('agent.customer.wallet.show', $wallet->number_account)->with('success', "Le contrat de crédit {$credit->reference} à été créer avec succès");
    }

    public function createEpargne($customer_id)
    {
        $customer = Customer::find($customer_id);

        return view('agent.customer.wallet.epargne.create', ['customer' => $customer]);
    }

    private function updateStatus(Customer $customer, Request $request)
    {
        if ($request->get('status_open_account') == 'closed') {
            DocumentFile::createDoc(
                $customer,
                'account close',
                'Cloture du compte',
                5,
                null,
                false,
                false,
                false,
                true
            );
        }
        try {
            $customer->update([
                'status_open_account' => $request->get('status_open_account')
            ]);

            $customer->user->notify(new UpdateStatusAccountNotification($customer, "Comptes & Moyens de paiement"));

        }catch (\Exception $exception) {
            LogHelper::notify('critical', $exception->getMessage());
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json(['status' => $customer->status_text]);
    }

    private function updateType(Customer $customer, Request $request)
    {
        if ($customer->package_id != $request->get('package_id')) {
            $customer->package_id = $request->get('package_id');
            $customer->save();

            $package = Package::find($request->get('package_id'));

            $doc = DocumentFile::createDoc(
                $customer,
                'convention part',
                'Avenant Contrat Particulier',
                3,
                null,
                true,
                true,
                true,
                true,
            );

            LogHelper::insertLogSystem('success', "Avenant à un contrat bancaire pour le client {$customer->info->full_name}", auth()->user());

            // Notification Client
            $customer->user->notify(new \App\Notifications\Customer\Customer\Customer\UpdateTypeAccountNotification($customer, $package, $doc->url_forlder));
        }
        return response()->json();
    }

    private function subscribeInsurance(Customer $customer, CustomerPret $pret, $assurance_type)
    {
        $insurance = $customer->insurances()->create([
            'reference' => generateReference(),
            'date_member' => now(),
            'effect_date' => now(),
            'end_date' => now()->addMonths($pret->duration),
            'mensuality' => CalcLoanInsuranceTrait::calcul($customer, $pret, $assurance_type)['mensuality'],
            'type_prlv' => 'mensuel',
            'beneficiaire' => null,
            'customer_id' => $customer->id,
            'customer_wallet_id' => $pret->wallet->id,
            'insurance_package_id' => 18,
            'insurance_package_form_id' => InsurancePackageForm::where('name', $assurance_type)->first()->id,
        ]);

        $contract = DocumentFile::createDoc(
            $customer,
            'insurance.contrat_assurance',
            'Contrat Assurance '.$insurance->package->name,
            1,
            $insurance->reference,
            true,
            true,
            false,
            true,
            ['insurance' => $insurance]
        );

        RequestHelper::create(
            $customer,
            "Signature d'un document",
            "Veuillez signer le document suivant: {$contract->name}",
            CustomerInsurance::class,
            $insurance->id,
        );

        DocumentFile::createDoc(
            $customer,
            'insurance.synthese_echange',
            'Synthese Echange '.$insurance->package->name,
            1,
            $insurance->reference,
            false,
            false,
            false,
            true,
            ['insurance' => $insurance]
        );

        DocumentFile::createDoc(
            $customer,
            'insurance.bordereau_retractation',
            'Bordereau de retractation',
            1,
            $insurance->reference,
            false,
            false,
            false,
            true,
            ['insurance' => $insurance]
        );

        return $insurance;
    }
}
