<?php

namespace App\Models\Customer;

use App\Helper\CustomerHelper;
use App\Helper\CustomerWithdrawHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Customer\CustomerMoneyDeposit
 *
 * @property int $id
 * @property string $reference
 * @property float $amount
 * @property string $status
 * @property string $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_wallet_id
 * @property int|null $customer_transaction_id
 * @property int $customer_withdraw_dab_id
 * @property-read \App\Models\Customer\CustomerWithdrawDab $dab
 * @property-read mixed $amount_format
 * @property-read mixed $customer_name
 * @property-read mixed $decoded_code
 * @property-read mixed $labeled_status
 * @property-read \App\Models\Customer\CustomerTransaction|null $transaction
 * @property-read \App\Models\Customer\CustomerWallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereCustomerTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereCustomerWalletId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereCustomerWithdrawDabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMoneyDeposit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CustomerMoneyDeposit extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = ['decoded_code', 'labeled_status', 'amount_format'];

    public function wallet()
    {
        return $this->belongsTo(CustomerWallet::class, 'customer_wallet_id');
    }

    public function transaction()
    {
        return $this->belongsTo(CustomerTransaction::class, 'customer_transaction_id');
    }

    public function dab()
    {
        return $this->belongsTo(CustomerWithdrawDab::class, 'customer_withdraw_dab_id');
    }

    public function getDecodedCodeAttribute()
    {
        return base64_decode($this->code);
    }

    public function getStatus($format = '')
    {
        if($format == 'text') {
            return match ($this->status) {
                "pending" => "En attente",
                "accepted" => "Accepté",
                "rejected" => "Rejeté",
                default => "Terminé"
            };
        } elseif ($format == 'color') {
            return match ($this->status) {
                "pending" => "warning",
                "rejected" => "danger",
                default => "success"
            };
        } else {
            return match ($this->status) {
                "pending" => "fa-spinner fa-spin-pulse",
                "rejected" => "fa-xmark-circle",
                default => "fa-check-circle"
            };
        }
    }

    public function getLabeledStatusAttribute()
    {
        return "<span class='badge badge-{$this->getStatus('color')}'><i class='fa-solid {$this->getStatus()} text-white me-2'></i> {$this->getStatus('text')}</span>";
    }

    public function getAmountFormatAttribute()
    {
        return eur($this->amount);
    }

    /**
     * @param float $amount
     * @param int $wallet_id
     * @param int $dab_id
     * @param int|null $transaction_id
     * @return Model|CustomerMoneyDeposit
     */
    public static function createDeposit(float $amount, int $wallet_id, int $dab_id, int $transaction_id = null,): Model|CustomerMoneyDeposit
    {
        $code = random_numeric(6);
        return self::create([
            'reference' => generateReference(),
            'amount' => $amount,
            'status' => 'pending',
            'customer_wallet_id' => $wallet_id,
            'customer_transaction_id' => $transaction_id,
            'customer_withdraw_dab_id' => $dab_id,
            'code' => base64_encode($code)
        ]);
    }
}
