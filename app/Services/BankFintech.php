<?php

namespace App\Services;

use App\Models\Core\Agency;
use App\Models\Customer\Customer;

class BankFintech
{
    private string $endpoint;

    public function __construct()
    {
        $this->endpoint = "https://bank.".config('app.app_domain')."/";
    }

    public function callRefundSepa($sepa)
    {
        return \Http::get($this->endpoint.'refund_request?bank_id=' . $sepa)->object();
    }

    public function callStatusBank($bank_name)
    {
        return \Http::get($this->endpoint.'status_request?bank_name=' . $bank_name)->object();
    }

    public function callInter()
    {
        return \Http::get($this->endpoint.'inter')->object();
    }

    public function callTransferDoc()
    {
        return \Http::timeout(50)->post($this->endpoint.'mobility/transfer_doc')->object();
    }

    public function callCreditorDoc()
    {
        return \Http::timeout(50)->post($this->endpoint.'mobility/creditor_doc')->object();
    }

    public function status()
    {
        return \Http::get($this->endpoint.'status')->status();
    }
}
