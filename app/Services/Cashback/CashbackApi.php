<?php

namespace App\Services\Cashback;

class CashbackApi
{
    private string $endpoint;

    public function __construct()
    {
        $this->endpoint = "https://cashback.".config('app.app_domain')."/api";
    }

    public function callWallet($customer_id)
    {
        return \Http::withoutVerifying()->post($this->endpoint.'/wallet', ["customer_id" => $customer_id])->object();
    }
}
