<?php

namespace App\Helper;

class CustomerWithdrawHelper
{
    public static function getStatusDab(int $open, bool $format = true): bool|string
    {
        if ($format) {
            if($open) {
                return "Ouvert";
            } else {
                return "Fermé";
            }
        } else {
            if($open) {
                return true;
            } else {
                return false;
            }
        }
    }
}
