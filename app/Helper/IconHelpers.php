<?php

namespace App\Helper;

class IconHelpers
{
    public static function getIcons()
    {
        return collect([
            "circle-info",
            "eye",
            "question",
            "phone-volume",
            "tty",
            "hands",
            "audio-description",
            "fingerprint",
            "wheelchair",
            "person-cane",
            "circle-question",
            "braille",
            "bell",
            "circle-exclamation",
            "question",
            "exclamation",
            "triangle-exclamation",
            "radiation",
            "bell-slash",
        ]);
    }
}
